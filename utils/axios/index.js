import BaseAxios from "axios";
import { apiKey, baseURL } from "../constants";
export const axios = BaseAxios.create({ baseURL })




axios.interceptors.request.use(
  (request) => {
    const newRequest = { ...request }
    newRequest.url += `&api_key=${apiKey}`
    return newRequest
  },
  (error) => {
    alert('Failed to connect to the server!')
    return Promise.reject(error)
  },
)

axios.interceptors.response.use(
  (response) => {
    return response.data
  },
  async (error) => {
    alert('Something went very wrong')
    return Promise.reject(error)
  },
)