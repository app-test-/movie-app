import { createReducer } from "@reduxjs/toolkit";
import {fetchFilms} from "./thunk";

const initialState = {
    films: [],
    suggestions:[],
    queryText: '',
    page:'',
    error: null,
    loading: false,
    loadingPagination:false
}



export default createReducer(initialState, builder =>
    builder.addCase(fetchFilms.pending, (state,{meta}) => {
        const { arg:{query,page}} = meta
        const pageState = page > 1
        state.loading = !pageState
        state.loadingPagination = pageState
        state.films =  state.queryText !== query ? [] : state.films
        state.error = null
    })
        .addCase(fetchFilms.fulfilled, (state, {payload,meta}) => {
            const  {results, page, totalPages, totalResults, suggestions} = payload
            const { arg:{query}} = meta
            const pageState = page > 1
            state.films = [...state.films,...results]
            state.page = page
            state.totalPages = totalPages
            state.totalResults = totalResults
            state.suggestions = suggestions
            state.queryText = query
            state.loading = !pageState ? false : state.loading
            state.loadingPagination = pageState ? false : state.loadingPagination
         })
        .addCase(fetchFilms.rejected, (state, { error,meta }) => {
            state.loading = false
            state.error = error.message 
        })

)
