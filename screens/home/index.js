import React, { useEffect, useCallback } from 'react'
import { useMemo } from 'react'
import { useState } from 'react'
import { View, Text, FlatList, TextInput, Keyboard, TouchableOpacity, ScrollView, ActivityIndicator, TouchableWithoutFeedback } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import FilmItem from '../../components/filmItem'
import styles from './styles'
import { fetchFilms } from './thunk'

const Home = () => {
    const { films, suggestions, page, loadingPagination, loading, totalPages } = useSelector(state => state.home)
    const dispatch = useDispatch()
    const [queryText, setQueryText] = useState('film')
    const [suggestionsState, setSuggestionsState] = useState(false)
    const searchFilms = () => {
        const searchText = queryText.trim()
        Keyboard.dismiss()
        setSuggestionsState(false)
        getFilms(searchText)
    }
    const searchBySuggestion = (suggestion) => {
        setSuggestionsState(false)
        setQueryText(suggestion)
        getFilms(suggestion)
    }

    const getFilms = useCallback(
        (query = 'film', page = 1) => dispatch(fetchFilms({ query, page })), [dispatch]
    )
    const buttonDisabled = useMemo(() => (!queryText || loading), [queryText, loading])
    const loadPaginatedFilms = () => {
        if (loadingPagination) return
        if (page + 1 > totalPages) return
        getFilms(queryText, page + 1)
    }
    useEffect(
        () => {
            getFilms()
        }, [getFilms]
    )
    return (
        <View style={styles.container}>

            <View style={styles.searchCont}>
                <TextInput
                    style={styles.input}
                    onChangeText={setQueryText}
                    onFocus={() => setSuggestionsState(true)}
                    value={queryText}
                    placeholder=" Search for a film"
                />
                <TouchableOpacity onPress={searchFilms} disabled={buttonDisabled} style={styles.button}>
                    <Text style={styles.buttonText}>
                        Search
                    </Text>
                </TouchableOpacity>
            </View>
            {suggestionsState && suggestions && (
                <ScrollView style={styles.suggestionCont}>
                    {suggestions.map((suggestion, idx) =>
                        <View key={idx.toString()}>
                            <TouchableOpacity key={idx.toString()}
                                style={styles.padding}
                                onPress={() => searchBySuggestion(suggestion)}>
                                <Text style={styles.suggestionText}>{suggestion}</Text>
                            </TouchableOpacity>
                            {idx < suggestions.length - 1 && <View style={styles.divider} />}
                        </View>
                    )}
                </ScrollView>
            )
            }

            { loading && <View style={styles.bigLoader}>
                <ActivityIndicator size="large" color="green" />
            </View>}
            { !loading && (
                <TouchableWithoutFeedback onPress={() => setSuggestionsState(false)}>
                    <FlatList
                        data={films}
                        keyExtractor={(item, idx) => idx + item.id.toString()}
                        renderItem={FilmItem}
                        onEndReached={loadPaginatedFilms}
                        onEndReachedThreshold={1}
                        ListFooterComponent={() => (
                            <ActivityIndicator size="large" color="green" />
                        )}
                    />
                </TouchableWithoutFeedback>
            )}

        </View>
    )
}

export default Home
