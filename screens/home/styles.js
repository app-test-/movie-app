import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: { flex: 1 },
    searchCont: {
        flexDirection: 'row',
        height: 50,
        padding: 5
    },
    input: {
        flex: 2
    },
    button: {
        padding: 10,
        backgroundColor: 'green',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: { color: 'white' },
    padding: { padding: 16 },
    bigLoader: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    suggestionText: {
        fontSize: 16
    },
    divider: { borderBottomWidth: 1, marginHorizontal: 16 },
    suggestionCont: { color: 'white' }


})