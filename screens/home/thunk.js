import { createAsyncThunk } from "@reduxjs/toolkit";
import { searchForFilm } from "../../api/movieApi";
import { imageBase } from "../../utils/constants";


const filterFilms = ({ id, title, overview, release_date, poster_path }) => id && title && overview && release_date && poster_path
const setRecent = (queryArray, query, numOfResults) => {
    if (queryArray.includes(query) || !numOfResults)
        return queryArray
    const newArray = [...queryArray]
    newArray.unshift(query)
    return newArray.filter((_, idx) => idx < 10)
}

const mapResults = (queryResults) => {
    const results = { ...queryResults }
    results.results = results.results.filter(filterFilms)
    results.results = results.results.map(({ id, title, overview, release_date, poster_path }) => ({
        id, title, overview,
        imageSrc: imageBase + poster_path, releaseDate: release_date
    }))
    return results
}

export const fetchFilms = createAsyncThunk('fetchFilm', async ({ query = 'batman', page = 1 }, thunkApi) => {
    try {
        const { page: currentPage, results, total_pages, total_results } = await searchForFilm(query, page)
        const currentSuggestions = thunkApi.getState().home.suggestions
        const lastSuggestions = setRecent(currentSuggestions, query, results.length)
        let queryResult = { page: currentPage, results, totalPages: total_pages, totalResults: total_results, suggestions: lastSuggestions }
        if (!queryResult.results.length) {
            alert('Error no such a film , try again!!!')
            return queryResult
        }
        queryResult = mapResults(queryResult)
        return queryResult
    } catch (error) {
        throw error
    }

}

)
