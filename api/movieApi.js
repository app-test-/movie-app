import { axios } from "../utils/axios"

export const searchForFilm = (query, page = 1) => {
    return axios.get(`movie?query=${query}&page=${page}`)
}
