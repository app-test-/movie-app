import { combineReducers } from "@reduxjs/toolkit";
import homeReducer from '../screens/home/reducer'


export const rootReducer = combineReducers({
    home: homeReducer,
})