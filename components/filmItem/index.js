import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './styles'

const FilmItem = ({ item: { title, overview, releaseDate, imageSrc } }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.header}>{title}</Text>
            <Image source={{ uri: imageSrc }} style={styles.image} resizeMode='contain' />
            <Text style={styles.description} >{overview}</Text>
            <Text style={styles.date} >{releaseDate}</Text>
        </View>
    )
}

export default FilmItem
