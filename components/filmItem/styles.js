import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        margin: 16,
        padding: 8,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    header: {
        paddingVertical: 4,
        fontSize: 16,
        fontWeight: "bold"
    },
    description: {
        paddingVertical: 8,
        fontStyle: 'italic'
    },
    image: {
        borderWidth: 1,
        height: 100,
        width: '100%'
    },
    date: {
        textAlign: 'right',
        color: 'gray'
    }

})