import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaView } from 'react-native';
import Home from '../screens/home';


const MainStack = createStackNavigator()


const MainNav = () => (
    <MainStack.Navigator screenOptions={{
        headerShown: false
    }}>
        <MainStack.Screen component={Home} name='name' />
    </MainStack.Navigator>
)

export default () => (
    <NavigationContainer>
        <SafeAreaView style={{ flex: 1 }}>
            <MainNav />
        </SafeAreaView>
    </NavigationContainer>
)